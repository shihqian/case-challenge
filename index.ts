import express, { Application, Request, Response } from "express";

export const app: Application = express();
const port = 3000;
const multer = require('multer');
const path = require('path');
const knex = require('knex');
require('dotenv').config();

const jimp = require('jimp');
const { v4: uuidv4 } = require('uuid');
const mime = require('mime-types');

export enum ImageType {
  JPEG = "jpeg",
  JPG = "jpeg",
  BMP = "BMP",
  PNG = "png",
  TIFF = "tiff",
}

//create DB object
const db = knex(
  {
    client: 'pg',
    connection: {
      host: '127.0.0.1',
      user: process.env.USER,
      password: process.env.PASSWORD,
      database: 'image_upload'
    }
  }
)


// create multer object
const imageUpload = multer({
  storage: multer.diskStorage(
    {
      destination: (req: any, file: any, cb: any) => {
        cb(null, 'images/');
      },
      filename: (req: any, file: any, cb: any) => {
        cb(
          null,
          new Date().valueOf() +
          '_' + uuidv4()
        )
      }
    }
  )
})

// Body parsing Middleware
app.use(express.json());
// app.use(morgan('dev'));

app.get('/', (req: Request, res: Response) => {
  return res.json(
    {
      success: true,
    })
});

//Image Upload Routes

app.post('/image', imageUpload.single('image'), (req: any, res: Response) => {

  const { filename, mimetype, size } = req.file;
  const filepath = req.file.path;
  db
    .insert({
      filename,
      filepath,
      mimetype,
      size,
    })
    .into('image_files')
    .then(() => res.json({ success: true, filename }))
    .catch((err: any) => res
      .json(
        {
          success: false,
          message: 'upload failed',
          stack: err.stack,
        }
      )
    );
})

// Image Get Routes
app.get('/image/:filename', (req: Request, res: Response) => {
  const { filename } = req.params;
  db
    .select('*')
    .from('image_files')
    .where({ filename })
    .then((images: any) => {
      if (images[0]) {
        const dirname = path.resolve();
        const fullfilepath = path.join(
          dirname,
          images[0].filepath);
        return res
          .type(images[0].mimetype)
          .sendFile(fullfilepath);
      }

      return Promise.reject(
        new Error('Image does not exist')
      );
    })
    .catch((err: any) => res
      .status(404)
      .json(
        {
          success: false,
          message: 'Image does not exist'
        }
      ),
    );
})

// Image Get Routes
app.get('/image/:filename/:extension', (req: Request, res: Response) => {
  const { filename, extension } = req.params;

  if (!Object.values(ImageType).includes(extension as ImageType)) {
    return res
      .status(404)
      .json(
        {
          success: false,
          message: 'Invalid image extension',
        }
      )
  }

  db
    .select('*')
    .from('image_files')
    .where({ filename })
    .then(async (images: any) => {
      let outputPath: any
      if (images[0]) {
        const dirname = path.resolve();
        outputPath = `./output/${filename}.${extension}`

        const image = await jimp.read(`./${images[0].filepath}`)
        await image.writeAsync(outputPath)
        const fullfilepath = path.join(
          dirname,
          outputPath);
        const mimeType = mime.contentType(path.extname(fullfilepath));

        return res
          .type(mimeType)
          .sendFile(fullfilepath);
      }
      return res
        .status(404)
        .json(
          {
            success: false,
            message: 'Image does not exist',
          }
        )
    })
    .catch((err: any) => {
      return res
        .status(404)
        .json(
          {
            success: false,
            message: 'Image does not exist',
          }
        )
    }
    );
})

try {
  app.listen(port, (): void => {
    console.log(`Connected successfully on port ${port}`);
  });
} catch (error: any) {
  console.error(`Error occured: ${error.message}`);
}

export default app