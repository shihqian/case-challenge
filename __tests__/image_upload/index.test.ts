import app from "../../index"
import request from "supertest"
const path = require('path')
describe("GET /", () => {
  test("should return success if connection is establish", async () => {

    const response = await request(app).get("/")
    expect(response.status).toBe(200)
    expect(response.body.success).toBeTruthy()
  })
})

describe("UPLOAD /image", () => {
  test("should return success if image is successfully uploaded", async () => {
    const dirname = path.resolve()
    const filePath = path.join(dirname, 'images/keycap_images.png');
    const response = await request(app).post("/image").attach('image', filePath)

    expect(response.status).toBe(200)
    expect(response.body.success).toBeTruthy()
    expect(response.body.filename).not.toBeNull()

  })
  test("should NOT return success if body key is incorrect", async () => {
    const dirname = path.resolve()
    const filePath = path.join(dirname, 'images/keycap_images.png');
    const response = await request(app).post("/image").attach('images', filePath)

    expect(response.status).toBe(500)
  })
})

describe("GET /image/:filename", () => {
  let uploadedfileName: string;
  const dirname = path.resolve()
  beforeAll(async () => {
    const filePath = path.join(dirname, 'images/keycap_images.png');
    const response = await request(app).post("/image").attach('image', filePath)
    uploadedfileName = response.body.filename
  })

  test("should return success if image is succesfully retrieve", async () => {
    const response = await request(app).get(`/image/${uploadedfileName}`)
    expect(response.header["content-type"]).toBe('image/png')
  })

  test("should NOT return success if image is NOT succesfully retrieve", async () => {
    const response = await request(app).get("/image/randomName")
    expect(response.body.status).toBeFalsy()
    expect(response.body.message).toBe('Image does not exist')
  })
})

describe("GET /image/:filename/:extension", () => {
  let uploadedfileName: string
  const dirname = path.resolve()
  beforeAll(async () => {
    const filePath = path.join(dirname, 'images/keycap_images.png');
    const response = await request(app).post("/image").attach('image', filePath)
    uploadedfileName = response.body.filename
  })

  test("should return image with correct extension, if image is succesfully retrieve and transpile", async () => {
    const extension = 'tiff'
    const response = await request(app).get(`/image/${uploadedfileName}/${extension}`)
    expect(response.header["content-type"]).toBe('image/tiff')
  })

  test("should NOT return image, if image extension is invalid", async () => {
    const extension = 'random'
    const response = await request(app).get(`/image/${uploadedfileName}/${extension}`)
    expect(response.body.status).toBeFalsy()
    expect(response.body.message).toBe('Invalid image extension')
  })

  test("should NOT return image, if image id is invalid", async () => {
    const extension = 'tiff'
    const wrongFileName = 'random'
    const response = await request(app).get(`/image/${wrongFileName}/${extension}`)
    expect(response.body.status).toBeFalsy()
    expect(response.body.message).toBe('Image does not exist')
  })

  test("should NOT return wrong extension, if image is succesfully retrieve and transpile", async () => {
    const extension = 'png'
    const response = await request(app).get(`/image/${uploadedfileName}/${extension}`)
    expect(response.header["content-type"]).not.toBe('image/tiff')
    expect(response.header["content-type"]).toBe('image/png')
  })
})