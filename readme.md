# Setup and Installation

```
yarn install
```

> Note: To create `.env` folder at root with db credentials, follow `Steps to create a db` if no db exists:

```
//.env
USER=<<USER>>
PASSWORD=<<USER>
```

### Steps to create a db

`Pre-requiste`: PostgreSQL needs to be installed, else refer to [How to use Postgresq](https://hseoy.medium.com/how-to-use-postgresql-on-linux-e8d12bc67c1b)

```
// In terminal -
createdb image_upload
psql image_upload

CREATE TABLE image_files(
    id SERIAL NOT NULL PRIMARY KEY,
    filename TEXT UNIQUE NOT NULL,
    filepath TEXT NOT NULL,
    mimetype TEXT NOT NULL,
    size BIGINT NOT NULL,
);

```

# Test

```
// In terminal -
yarn run test

// for coverage
yarn run test-coverage
```
